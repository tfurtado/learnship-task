# Learnship coding task

To install dependencies, download [Composer](http://getcomposer.org) and then:

    $ php composer.phar install

The database structure can be created with:

    $ php app/console doctrine:schema:create

Data should be inserted manually, since there is no administrative interface. A sample data file can be found in `doc/sample_data.sql`.

To run the code using PHP built-in web server:

    $ cd web
    $ php -S localhost:50825

And point the browser to: http://localhost:50825/app_dev.php
