# Description
Create a RESTful API endpoint which expects a list of magazines as an argument to mark them as sold out or pre-order possible. This has to be stored to the db somehow. Additionally, there should be a function which expects a list of either magazines or books as an argument and sets a rebate for those. Both functions do not need to consist of actual PHP code; but the structure and arguments, etc. is important.

# Solution

## First endpoint: mark sold-out/pre-order possible
My design choice was to use a *status* field in the *Item* entity. Thus, the endpoint is used to update that field.

### Request
The endpoint is designed to receive a *PATCH* message with a JSON body with the status to set on items filtered using query parameters. Ex:

```
PATCH /dealers/{dealerId}/items/status?publication\_id[]=1&publication\_id[]=2&...&publication\_id[]=N
Content-Type: application/json

{
    "status": "sold-out"
}
```

Valid values for `status` property are `sold-out`, `pre-order` and `available`.
Another possibility would be to receive the list in the body, e.g.:
```
{
    "status": "sold-out",
    "publication_id": [1,2,...,N]
}
```

### Response
The response body contains the status to which the items where updated to and how many of them were affected. Ex:
```
HTTP 1.1 200 OK
Content-Type: application/json

{
    "status": "sold-out",
    "updated_publications": 2
}
```

While processing, the publications are actually updated in the database. This is the code to achieve that, using Doctrine Query Builder:
```
$em   = $this->getDoctrine()->getManager();
$qb   = $em->getRepository(Item::class)->createQueryBuilder('i');
$stmt = $qb->update()
           ->set('i.status', ':status')
           ->where($qb->expr()->in('i.publication', ':publications'))
           ->andWhere('i.dealer = :dealer')
           ->setParameter(':status', $status)
           ->setParameter(':publications', $publications)
           ->setParameter(':dealer', $dealerId)
           ->getQuery();

// In actual code, there is a try-catch block here
$stmt->execute();
```

## Second endpoint: set rebate
My design choice was to update the *price* property in *Item* entity. The endpoint thus operates on this field.

### Request
I designed the endpoint to receive a *PATCH* message with a JSON body with the rebate to apply on items filtered using query parameters. Ex:

```
PATCH /dealers/{dealerId}/items/price?publication\_id[]=1&publication\_id[]=2&...&publication\_id[]=N
Content-Type: application/json

{
    "apply_rebate": 0.1
}
```

Valid values for `apply_rebate` property are the numbers within [0..1]. It represents the percentage of the price to rebate.

### Response
The response body contains the rebate applied and how many items were affected. Ex:
```
HTTP 1.1 200 OK
Content-Type: application/json

{
    "rebate": 0.1,
    "updated_publications": N
}
```

While processing, items' prices are actually changed in the database. This is the code to make the change, using Doctrine Query Builder:
```
$em   = $this->getDoctrine()->getManager();
$qb   = $em->getRepository(Item::class)->createQueryBuilder('i');
$stmt = $qb->update()
           ->set('i.price', 'i.price * (1 - :rebate)')
           ->where($qb->expr()->in('i.publication', ':publications'))
           ->andWhere('i.dealer = :dealer')
           ->setParameter(':rebate', $rebate)
           ->setParameter(':publications', $publications)
           ->setParameter(':dealer', $dealerId)
           ->getQuery();
           
// In actual code, there is a try-catch block here
$stmt->execute();
```