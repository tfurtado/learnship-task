# Learnship Coding Task

The assigned tasks were completed and the resulting solutions can be found in files in the current directory.

Each task is in a separate file: `task-1.md`, `task-2.md`, `task-3.md`, `task-4.md` and `task-5.md`.

I implemented a slightly different version of the code presented in the solution files and a git repository with it can be found in: https://bitbucket.org/tfurtado/learnship-task

There is a live version of the software, running in a Digital Ocean droplet. The view from task #5 can be found here: http://learnship.tiagofurtado.com:50825
