# Description
Aside from that, there has to be a function which creates prices from "rounded" Euro prices, e.g. it should make 4.99 EUR out of 5.00 EUR and 19.99 out of 20.00 EUR. If a parameter has been set, this function needs to also turn prices like 5.10 EUR into 4.99 (or 4.39 into 3.99 etc.). Here, the actual running code is necessary.

# Solution

The code to achieve the desired result follows: 

```
/**
 * @param float $price
 * @param bool  $forceRound
 *
 * @return string
 */
function formatFromRounded(float $price, bool $forceRound = false)
{
    if ($forceRound) {
        $price = (int)$price;
    }

    if ($price > 0 && $price * 100 % 100 == 0) {
        $price -= 0.01;
    }

    return sprintf('%.2f', $price);
}
```
