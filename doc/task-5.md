# Description
Additionally, there should be a view with jQuery which retrieves a list of book dealers via AJAX from the DB (using a RESTful API). Every odd column should have a different color. If I click on a dealer in the list, an AJAX call should retrieve the dealer's books and magazines. The JavaScript code is important, but you can use dummy HTML code.

# Solution
The HTML file can be found inside `view` folder in current directory.