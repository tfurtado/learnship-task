# Description
There should be a RESTful API endpoint which retrieves the average prices of books or magazines and books and magazines from the dealers. Here the actual SQL is needed.

# Solution

SQL code to retrieve the average prices of all publications (books and magazines):

```
SELECT p.id, p.title, p.type, AVG(i.price) AS average_price
FROM item AS i JOIN publication AS p ON i.publication_id=p.id
GROUP BY p.id;
```

SQL code to retrieve the average prices of books:

```
SELECT p.id, p.title, p.type, AVG(i.price) AS average_price
FROM item AS i JOIN publication AS p ON i.publication_id=p.id
WHERE p.type='Book'
GROUP BY p.id;
```

SQL code to retrieve the average prices of magazines:

```
SELECT p.id, p.title, p.type, AVG(i.price) AS average_price
FROM item AS i JOIN publication AS p ON i.publication_id=p.id
WHERE p.type='Magazine'
GROUP BY p.id;
```

Here is a version using Doctrine's Query Builder:

```
$em   = $this->getDoctrine()->getManager();
$qb   = $em->getRepository(Item::class)->createQueryBuilder('i');
$stmt = $qb->select('p.id, p.title, AVG(i.price) AS average_price')
           ->join('i.publication', 'p')
           ->groupBy('i.publication');
if ($type !== null) {
    $stmt->where('p INSTANCE OF :type')
         ->setParameter('type', $type);
}

$stmt->getQuery()->execute();
```