# Description
Task: please use Symfony 2.x with Propel (or Doctrine you are free to choose) to manage book dealers. There are magazines with titles, dates and prices, as well as books with publishers, titles and prices (it's possible that there is the same book at different dealers), so try to create a db schema with as less redundancy as possible as well as constraints where possible. Create a schema.xml (or doctrine/[entity name].yml) file(s) for the db.

# Solution
Files are located inside `doctrine` folder in current directory.
