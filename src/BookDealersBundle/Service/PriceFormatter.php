<?php
/**
 * This file is part of learnship
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * (c) Tiago Furtado <tfurtado@gmail.com>
 */
namespace BookDealersBundle\Service;

/**
 * @author Tiago Furtado <tfurtado@gmail.com>
 */
class PriceFormatter
{
    /**
     * @param float $price
     * @param bool  $forceRound
     *
     * @return string
     */
    public function formatFromRounded(float $price, bool $forceRound = false)
    {
        if ($forceRound) {
            $price = (int)$price;
        }

        if ($price > 0 && $price * 100 % 100 == 0) {
            $price -= 0.01;
        }

        return $this->getFormatedPrice($price);
    }

    /**
     * @param float $price
     *
     * @return string
     */
    public function getFormatedPrice(float $price)
    {
        return sprintf('%.2f', $price);
    }
}
