<?php
/**
 * This file is part of learnship
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * (c) Tiago Furtado <tfurtado@gmail.com>
 */

namespace BookDealersBundle\Entity;

/**
 * @author Tiago Furtado <tfurtado@gmail.com>
 */
class Magazine extends Publication
{
    /**
     * @var \DateTime
     */
    private $date;

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set date
     *
     * @param string $date
     *
     * @return Magazine
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return parent::jsonSerialize()
               + [
                   'date' => $this->date->format('Y-m-d'),
                   'type' => 'Magazine',
               ];
    }
}
