<?php

namespace BookDealersBundle\Entity;

/**
 * @author Tiago Furtado <tfurtado@gmail.com>
 */
class Item implements \JsonSerializable
{
    /**
     * @var Dealer
     */
    private $dealer;

    /**
     * @var Publication
     */
    private $publication;

    /**
     * @var string
     */
    private $price;

    /**
     * @var string
     */
    private $status;

    /**
     * Get dealer
     *
     * @return Dealer
     */
    public function getDealer()
    {
        return $this->dealer;
    }

    /**
     * Set dealer
     *
     * @param Dealer $dealer
     *
     * @return Item
     */
    public function setDealer(Dealer $dealer = null)
    {
        $this->dealer = $dealer;

        return $this;
    }

    /**
     * Get publication
     *
     * @return Publication
     */
    public function getPublication()
    {
        return $this->publication;
    }

    /**
     * Set publication
     *
     * @param Publication $publication
     *
     * @return Item
     */
    public function setPublication(Publication $publication)
    {
        $this->publication = $publication;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Item
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Item
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return $this->publication->jsonSerialize()
               + [
                   'price'  => $this->price,
                   'status' => $this->status,
               ];
    }
}
