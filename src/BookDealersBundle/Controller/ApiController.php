<?php
/**
 * This file is part of learnship
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * (c) Tiago Furtado <tfurtado@gmail.com>
 */

namespace BookDealersBundle\Controller;

use BookDealersBundle\Entity\Dealer;
use BookDealersBundle\Entity\Item;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Tiago Furtado <tfurtado@gmail.com>
 *
 * @Route("/api")
 */
class ApiController extends Controller
{
    /**
     * @param int     $dealerId
     * @param Request $request
     *
     * @return JsonResponse
     * @Route("/dealers/{dealerId}/items/status", methods={"PATCH"}, requirements={"dealerId": "\d+"})
     */
    public function updateItemsStatus(int $dealerId, Request $request)
    {
        if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
            $data = json_decode($request->getContent(), true);
            $request->request->replace(is_array($data) ? $data : []);
        }

        $status        = $request->get('status');
        $validStatuses = ['sold-out', 'pre-order', 'available'];
        if (!in_array($status, $validStatuses)) {
            return new JsonResponse(
                [
                    'message'      => "Invalid status.",
                    'valid_values' => $validStatuses,
                ],
                JsonResponse::HTTP_NOT_ACCEPTABLE
            );
        }

        $publications = $request->query->get('publication_id');
        if (!is_array($publications)) {
            $publications = explode(',', $publications);
        }

        /** @var EntityManager $em */
        $em   = $this->getDoctrine()->getManager();
        $qb   = $em->getRepository(Item::class)->createQueryBuilder('i');
        $stmt = $qb->update()
                   ->set('i.status', ':status')
                   ->where($qb->expr()->in('i.publication', ':publications'))
                   ->andWhere('i.dealer = :dealer')
                   ->setParameter(':status', $status)
                   ->setParameter(':publications', $publications)
                   ->setParameter(':dealer', $dealerId)
                   ->getQuery();

        try {
            $result = $stmt->execute();

            return new JsonResponse(
                [
                    'status'               => $status,
                    'updated_publications' => $result,
                ]
            );
        } catch (\Exception $e) {
            return new JsonResponse(['message' => $e->getMessage()], 500);
        }
    }

    /**
     * @param int     $dealerId
     * @param Request $request
     *
     * @return JsonResponse
     * @Route("/dealers/{dealerId}/items/price", methods={"PATCH"}, requirements={"dealerId": "\d+"})
     */
    public function applyRebateOnItems(int $dealerId, Request $request)
    {
        if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
            $data = json_decode($request->getContent(), true);
            $request->request->replace(is_array($data) ? $data : []);
        }

        $rebate = $request->get('apply_rebate', -1);
        if ($rebate < 0 || 1 < $rebate) {
            return new JsonResponse(
                [
                    'message'      => "Invalid rebate.",
                    'valid_values' => '[0..1]',
                ],
                JsonResponse::HTTP_NOT_ACCEPTABLE
            );
        }

        $publications = $request->query->get('publication_id');
        if (!is_array($publications)) {
            $publications = explode(',', $publications);
        }

        /** @var EntityManager $em */
        $em   = $this->getDoctrine()->getManager();
        $qb   = $em->getRepository(Item::class)->createQueryBuilder('i');
        $stmt = $qb->update()
                   ->set('i.price', 'i.price * (1 - :rebate)')
                   ->where($qb->expr()->in('i.publication', ':publications'))
                   ->andWhere('i.dealer = :dealer')
                   ->setParameter(':rebate', $rebate)
                   ->setParameter(':publications', $publications)
                   ->setParameter(':dealer', $dealerId)
                   ->getQuery();

        try {
            $result = $stmt->execute();

            return new JsonResponse(
                [
                    'rebate'               => $rebate,
                    'updated_publications' => $result,
                ]
            );
        } catch (\Exception $e) {
            return new JsonResponse(['message' => $e->getMessage()], 500);
        }
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @Route("/items/average-price", methods={"GET"})
     */
    public function getAveragePrices(Request $request)
    {
        $validTypes = ['Book', 'Magazine'];

        $type = $request->get('type');
        if ($type !== null && !in_array($type, $validTypes)) {
            return new JsonResponse(
                [
                    'message'       => "Invalid type.",
                    'invalid_value' => $type,
                    'valid_values'  => $validTypes,
                ],
                JsonResponse::HTTP_NOT_ACCEPTABLE
            );
        }

        /** @var EntityManager $em */
        $em   = $this->getDoctrine()->getManager();
        $qb   = $em->getRepository(Item::class)->createQueryBuilder('i');
        $stmt = $qb->select('p.id, p.title, AVG(i.price) AS average_price')
                   ->join('i.publication', 'p')
                   ->groupBy('i.publication');
        if ($type !== null) {
            $stmt->where('p INSTANCE OF :type')
                 ->setParameter('type', $type);
        }

        return new JsonResponse($stmt->getQuery()->execute());
    }

    /**
     * @return JsonResponse
     * @Route("/dealers", methods={"GET"})
     */
    public function getDealers()
    {
        $repository = $this->getDoctrine()->getManager()->getRepository(Dealer::class);

        return new JsonResponse($repository->findAll());
    }

    /**
     * @param int $dealerId
     *
     * @return JsonResponse
     * @Route("/dealers/{dealerId}/publications", methods={"GET"}, requirements={"dealerId": "\d+"})
     */
    public function getPublications(int $dealerId)
    {
        /** @var EntityManager $em */
        $em   = $this->getDoctrine()->getManager();
        $qb   = $em->getRepository(Item::class)->createQueryBuilder('i');
        $stmt = $qb->select()
                   ->join('i.publication', 'p')
                   ->where('i.dealer = :dealer')
                   ->setParameter('dealer', $dealerId)
                   ->getQuery();

        return new JsonResponse($stmt->execute());
    }
}
